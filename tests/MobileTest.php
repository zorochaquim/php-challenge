<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider 	= m::mock('App\Interfaces\CarrierInterface');
		$mobile 	= new Mobile($provider);

		$this->assertNull($mobile->makeCallByName());
	}

	/** @test */
    public function it_returns_not_null_when_name_not_empty()
    {
        $provider 	= m::mock('App\Interfaces\CarrierInterface');
		$mobile 	= new Mobile($provider);
		
        $this->assertNotNull($mobile->makeCallByName('Manuel'));
	}

	/** @test */
	public function it_returns_true_when_name_is_verify()
    {
        $provider 	= m::mock('App\Interfaces\CarrierInterface');
		$mobile 	= new Mobile($provider);
		
        $this->assertTrue($mobile->verifyContac('Manuel'));
	}
	
	/** @test */
	public function it_returns_true_when_send_sms()
    {
        $provider 	= m::mock('App\Interfaces\CarrierInterface');
		$mobile 	= new Mobile($provider);
		
        $this->assertTrue($mobile->sendSms("999999999", "text"));
	}

	/** @test */
	public function it_returns_true_verify_operator_Carrier()
    {
        $provider 	= m::mock('App\Interfaces\CarrierInterface');
		$mobile 	= new Mobile($provider);
		
        $this->assertTrue($mobile->operatorCarrier("entel"));
    }

}
