<?php

namespace App;


class Contact
{
	private $contact;
	public $name;
	public $cellphone;
	public $verifyContac;
	function __construct()
	{	
		$this->verifyContac = false;
		$this->contact = array(
           	"primero" => array(
				'name' => 'Manuel', 'cellphone' => '999999999'
			),
			"segundo" => array(
				'name' => 'Alejandro', 'cellphone' => '88888888'
			)	
        );
	}
	public function find($name = null, $number = null){
		$valid = false;

		foreach ($this->contact as $key => $value) {

			if($name != null){
				$valid = $value["name"] == $name;
			}else if($number != null){
				$valid = $value["cellphone"] == $number;
			}

			if($valid){
				$this->name 		= $value["name"];
				$this->cellphone 	= $value["cellphone"];
				$this->verifyContac = true;
			}
		}
	}

	public function verify(){
		return $this->verifyContac;
	}
}