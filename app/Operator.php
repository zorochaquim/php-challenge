<?php

namespace App;

use App\Contact;
use App\Call;
use App\Interfaces\CarrierInterface;

class Operator implements CarrierInterface {
  
  function dialContact(Contact $contact) {
    
  }

  function sendSms($number, $text) {
    return true;
  }

	function makeCall(): Call {
    return new Call();
  }
  
}  
