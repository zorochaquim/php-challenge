<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
use App\OperatorCarrier;
use App\Operator;

class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact 	= ContactService::findByName($name);
		$operator 	= new Operator();
		$operator->dialContact($contact);

		return $operator->makeCall();
	}

	public function verifyContac($name = '')
	{
		if( empty($name) ) return;

		$contact 	= ContactService::findByName($name);
		return $contact->verify();
	}

	public function sendSms($number = '', $text = '')
	{
		if( empty($number) OR empty($text) ) return false;
		$status 			= false;
		$validateNumber 	= ContactService::validateNumber($number);
		$operator 			= new Operator();
		if($validateNumber){
			$operator->sendSms($number, $text);
			$status = true;
		}
		
		return $status;
	}

	public function operatorCarrier($valoperator)
	{
		if( empty($valoperator) ) return false;
		$status 			= false;
		$operator 			= new OperatorCarrier();
		$operator->find($valoperator);
		return $operator->verify();
	}

}
